# Twitch app

Twitch app est un de mes projets React JS avec le package Axios.\
Ce projet résulte d'une semaine de formation dispensée par Enzo Ustariz sur la plateforme de formation en ligne Udemy.

**Prérequis**

Pour pouvoir exécuter ce projet sur ton environnement, il te faut d'abord installer quelques outils.\
Ceux-ci sont listés ci-dessous :

- NodeJS/NPM ([Installation](https://nodejs.org/en/))
- Yarn ([Installation](https://yarnpkg.com/getting-started/install))
- Une clé API de Twitch ([Documentation](https://dev.twitch.tv/console))

## Exécuter le projet

Dans ce projet, je fais des appels API de la plateforme de streaming Twitch.
J'utilise dans ce projet quelques outils autour de l'univers de React, notamment :

- Les React Hooks (useState, useEffect, etc.)
- React-router, pour simuler une navigation, ainsi que les Hooks inclus dans React-router.
- Material UI
- Le package de requête HTTP Axios

**NOTE** : Supposons que tu es à la racine du projet. (`PWD == ./twitch_app`)

Il faut tout d'abord coller la clé API que tu as récupérée sur le site de [Twitch](https://dev.twitch.tv/console) dans le fichier `api.js` qui se trouve `src/util/api.js`. Remplace la valeur de la constante `CLIENT_ID` pour ta clé.

Maintenant, que la clé a été mise en place, il faut exécuter le projet et y accéder via un navigateur. Pour cela, tape juste la commande ci-dessous dans ton terminal.
``` bash
$ yarn install 
$ yarn start
```
ou
``` bash
$ npm install 
$ npm run start
```
**INFO** : N'hésite pas à mettre les packages à jour si tu souhaites t'amuser 🙄 !

Après quelques instants, tu devrais pouvoir accéder au site à l'adresse : [http://localhost:3000/](http://localhost:3000/).