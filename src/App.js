import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { makeStyles } from '@material-ui/core/styles'
import CssBaseline from '@material-ui/core/CssBaseline'
import Sidebar from './components/Sidebar/Sidebar'
import Home from './pages/Home/Home'
import Navbar from './components/Navbar/Navbar'
import Stream from './pages/Stream/Stream'
import LiveStream from './pages/LiveStream/LiveStream'
import GameStream from './pages/GameStream/GameStream'
import Result from './pages/Result/Result'

function App ({ streams, games, users }) {

    const drawerWidth = 240

    const useStyles = makeStyles(theme => ({
        root: {
            display: 'flex',
        },
        appBar: {
            width: `calc(100% - ${drawerWidth}px)`,
            marginLeft: drawerWidth,
            backgroundColor: "#26222C",
        },
        drawer: {
            width: drawerWidth,
            flexShrink: 0,
        },
        drawerPaper: {
            width: drawerWidth,
            backgroundColor: "#6641A6",
            border: 0,
        },
        // necessary for content to be below app bar
        toolbar: theme.mixins.toolbar,
        content: {
            flexGrow: 9999,
            padding: theme.spacing(3),
        },
        media: {
            height: 140,
        },
    }));

    const classes = useStyles();

    return (
        <Router>
            <div className="App">
                <React.Fragment>
                    <div className={classes.root}>
                        <CssBaseline/>
                        <Navbar position="fixed" className={classes.appBar}/>
                        <Sidebar className={classes.drawer} variant="permanent"
                                 classes={{paper: classes.drawerPaper}} anchor="left"/>
                        <Switch>
                            <main className={classes.content}>
                                <div className={classes.toolbar}/>
                                <Route exact path="/" component={Home}/>
                                <Route exact path="/streams" component={Stream}/>
                                <Route exact path="/live/:slug" component={LiveStream}/>
                                <Route exact path="/games/:slug" component={GameStream}/>
                                <Route exact path="/result/:slug" component={Result}/>
                            </main>
                        </Switch>
                    </div>
                </React.Fragment>
            </div>
        </Router>
    );
}

export default App;
