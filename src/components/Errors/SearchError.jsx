import React from "react";

function SearchError() {
    return (
        <React.Fragment>
            <h3>Error : Player name is misspelled or does not exist</h3>
        </React.Fragment>
    )
}

export default SearchError
