import React, {useEffect, useState} from "react";
import {makeStyles} from '@material-ui/core/styles';
import api from "../../util/api";
import DefaultSkeleton from "../Skeleton/DefaultSkeleton";
import {Card, CardActionArea, CardMedia, Grid, Paper, Typography} from "@material-ui/core";
import {Link} from "react-router-dom";

function TopGame() {

    const useStyles = makeStyles({
        root: {
            maxWidth: 188,
        },
        media: {
            height: 250,
        },
    });

    const classes = useStyles();

    const [topGames, setTopGames] = useState([]);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        const fetchTopGames = async () => {
            const result = await api.get("https://api.twitch.tv/helix/games/top");
            let data = result.data.data;
            let finalData = data.map(topGame => {
                let newUrl = topGame.box_art_url
                    .replace("{width}", "188")
                    .replace("{height}", "250");
                topGame.box_art_url = newUrl;
                return topGame;
            });

            setTopGames(finalData.slice(0, 6));
            setLoading(false);
        };

        fetchTopGames()
    }, []);

    // Init the skeleton of ads
    const loaderNumbers = [1, 2, 3, 4, 5, 6];
    const loaderItems = loaderNumbers.map((loader) => <DefaultSkeleton key={loader.toString()}/>);

    return (
        <React.Fragment>
            {loading ?
                <React.Fragment>
                    <Grid container spacing={1}>
                        {loaderItems}
                    </Grid>
                </React.Fragment>
                :
                <React.Fragment>
                    <Grid container spacing={1}>
                        {topGames.map((topGame, index) => (
                            <Grid item sm={2} xs={12} key={index}>
                                <Link to={{
                                    pathname: "games/" + topGame.name,
                                    state: {
                                        gameId: topGame.id
                                    }
                                }}>
                                    <Card className={classes.root}>
                                        <CardActionArea>
                                            <CardMedia
                                                component="img"
                                                alt={topGame.name}
                                                image={topGame.box_art_url}
                                                title={topGame.name}
                                            />
                                        </CardActionArea>
                                    </Card>
                                </Link>
                                <Paper className="mt-2 bg-transparent" elevation={0}>
                                    <Typography variant={"h6"} style={{color: "#fff", fontWeight: 400, fontSize: 13}}>
                                        {topGame.name}
                                    </Typography>
                                    <Typography
                                        variant={"caption"}
                                        className="text-muted">
                                        Watch the game >
                                    </Typography>
                                </Paper>
                            </Grid>
                        ))}
                    </Grid>
                </React.Fragment>
            }
        </React.Fragment>
    )
}

export default TopGame;
