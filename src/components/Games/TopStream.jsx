import React from "react";
import {Card, CardActionArea, CardMedia, Grid, makeStyles, Paper, Typography} from "@material-ui/core";

function TopStream({image, title, name, viewer}) {

    const useStyles = makeStyles({
        root: {
            maxWidth: 345,
        },
        media: {
            height: 115,
        },
    });

    const classes = useStyles();

    return (
        <React.Fragment>
            <Grid item xs={12} sm={1}>
                <Card>
                    <CardActionArea>
                        <CardMedia
                            className={classes.media}
                            image={image}
                            title={title}
                        />
                    </CardActionArea>
                </Card>
                <Paper className="mt-2 bg-transparent" elevation={0}>
                    <Typography variant={"h6"} style={{
                        color: "#fff",
                        fontWeight: 400,
                        fontSize: 13
                    }}>{name}</Typography>
                    <Typography
                        variant={"caption"}
                        className="text-muted">
                        {viewer} Viewers</Typography>
                </Paper>
            </Grid>
        </React.Fragment>
    )
}

export default TopStream
