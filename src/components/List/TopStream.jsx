import React, {useEffect, useState} from "react";
import {Avatar, List, ListItem, ListItemAvatar, ListItemText, makeStyles, Typography} from '@material-ui/core';
import api from "../../util/api";
import getGameData from "../../util/getGameData";

function TopStream() {

    const useStyles = makeStyles((theme) => ({
        root: {
            width: '100%',
            maxWidth: '36ch',
            backgroundColor: "none",
        },
        inline: {
            display: 'block',
        },
        summary: {
            overflow: 'hidden'
        }
    }));

    const [topStreams, setTopStreams] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            const getGamesData = getGameData;

            const result = await getGamesData.getStreams();
            let dataArray = result;
            //console.log(result);

            let gameIDs = dataArray.map(stream => {
                return stream.game_id;
            });

            let userIDs = dataArray.map(stream => {
                return stream.user_id;
            });
            //console.log(gameIDs, userIDs);

            let baseUrlGames = getGamesData.getGames();
            let baseUrlUsers = getGamesData.getUsers();

            let queryParamsGame = "";
            let queryParamsUser = "";

            gameIDs.map(id => {
                return (queryParamsGame = queryParamsGame + `id=${id}&`)
            });

            userIDs.map(id => {
                return (queryParamsUser = queryParamsUser + `id=${id}&`)
            });

            let finalGameUrls = baseUrlGames + queryParamsGame;
            let finalUserUrls = baseUrlUsers + queryParamsUser;

            //console.log(finalGameUrls, finalUserUrls);

            let gameNames = await api.get(finalGameUrls);
            let getUsers = await api.get(finalUserUrls);

            let gamesNameArray = gameNames.data.data;
            let usersArray = getUsers.data.data;

            //console.log(gamesNameArray, usersArray)

            let finalArray = dataArray.map(stream => {
                stream.game_name = "";
                stream.user_avatar = "";
                stream.box_art_url = "";
                stream.login = "";

                gamesNameArray.forEach(name => {
                    usersArray.forEach(user => {
                        if (stream.user_id === user.id && stream.game_id === name.id) {
                            stream.user_avatar = user.profile_image_url;
                            stream.box_art_url = name.box_art_url
                                .replace("{width}", "188")
                                .replace("{height}", "250");
                            stream.game_name = name.name;
                            stream.login = user.login
                        }
                    })
                });
                return stream
            });

            setTopStreams(finalArray.slice(0, 6))
        };

        fetchData();
    }, []);

    const classes = useStyles();

    return (
        <React.Fragment>
            {topStreams.map((topStream, index) => (
                <List className={classes.root} key={index} style={{paddingTop: 0, paddingBottom: 0}}>
                    <ListItem alignItems="flex-start" button style={{paddingTop: 0, paddingBottom: 0}}>
                        <ListItemAvatar>
                            <Avatar style={{backgroundColor: "#9A80CD"}} alt={`User avatar of ` + topStream.login}
                                    src={topStream.user_avatar}/>
                        </ListItemAvatar>
                        <ListItemText
                            style={{color: "#fff", fontWeight: "800 !important"}}
                            primary={topStream.login}
                            secondary={
                                <React.Fragment>
                                    <Typography
                                        component="span"
                                        variant="caption"
                                        className={classes.inline + ' ' + classes.summary}
                                        style={{color: "#fff"}}
                                    >
                                        {topStream.viewer_count} Viewers
                                    </Typography>
                                    <Typography
                                        component="span"
                                        variant="caption"
                                        className={classes.inline}
                                        style={{color: "#9A80CD"}}
                                    >
                                        {topStream.game_name}
                                    </Typography>
                                </React.Fragment>
                            }
                        />
                    </ListItem>
                </List>
            ))}
        </React.Fragment>
    )
}

export default TopStream;
