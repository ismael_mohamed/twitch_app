import React, {useState} from "react";
import {fade, makeStyles} from '@material-ui/core/styles';
import {Badge, Button, IconButton, InputBase, Toolbar} from "@material-ui/core";
import AppBar from "@material-ui/core/AppBar";
import {AccountCircle, Mail, Menu, More, Notifications, Search} from '@material-ui/icons';
import {Link} from "react-router-dom";

function Navbar({className, position}) {

    const [searchInput, setSearchInput] = useState('');

    const useStyles = makeStyles(theme => ({
        grow: {
            flexGrow: 1,
        },
        menuButton: {
            marginRight: theme.spacing(2),
            '&:focus': {
                border: "none"
            }
        },
        button: {
            margin: theme.spacing(1),
            '&focus': {
                border: "0px"
            }
        },
        title: {
            display: 'none',
            [theme.breakpoints.up('sm')]: {
                display: 'block',
            },
        },
        search: {
            position: 'relative',
            borderRadius: theme.shape.borderRadius,
            backgroundColor: "#1E1D22",
            '&:hover': {
                backgroundColor: "#1A191E",
            },
            marginRight: theme.spacing(2),
            marginLeft: 0,
            width: '100%',
            [theme.breakpoints.up('sm')]: {
                marginLeft: theme.spacing(3),
                width: 'auto',
            },
        },
        searchIcon: {
            padding: theme.spacing(0, 2),
            height: '100%',
            position: 'absolute',
            pointerEvents: 'none',
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center',
        },
        inputRoot: {
            color: 'inherit',
        },
        inputInput: {
            padding: theme.spacing(1, 1, 1, 0),
            // vertical padding + font size from searchIcon
            paddingLeft: `calc(1em + ${theme.spacing(0)}px)`,
            transition: theme.transitions.create('width'),
            width: '100%',
            [theme.breakpoints.up('md')]: {
                width: '20ch',
            },
        },
        sectionDesktop: {
            display: 'none',
            [theme.breakpoints.up('md')]: {
                display: 'flex',
            },
        },
        sectionMobile: {
            display: 'flex',
            [theme.breakpoints.up('md')]: {
                display: 'none',
            },
        },
    }));

    const classes = useStyles();

    const handleSubmit = (e) => {
        e.preventDefault();
    };

    const handleChange = (e) => {
        setSearchInput(e.target.value)
    };

    return (
        <div className={classes.grow}>
            <AppBar elevation={0} position={position} className={className} style={{zIndex: "999999999"}}>
                <Toolbar>
                    <IconButton
                        edge="start"
                        className={classes.menuButton}
                        color="inherit"
                        aria-label="open drawer"
                    >
                        <Menu/>
                    </IconButton>
                    <form onSubmit={handleSubmit}>
                        <div className={classes.search}>
                            <InputBase
                                required
                                value={searchInput}
                                onChange={(e) => handleChange(e)}
                                type="text"
                                placeholder="Search…"
                                classes={{
                                    root: classes.inputRoot,
                                    input: classes.inputInput,
                                }}
                                inputProps={{'aria-label': 'search'}}
                            />
                            <Link to={{
                                pathname: `/result/${searchInput}`
                            }}>
                                <Button
                                    size={"small"}
                                    variant="contained"
                                    className={classes.button}
                                    style={{ backgroundColor: "#6641A6", color: "#fff" }}
                                    startIcon={<Search/>}
                                >
                                    Search
                                </Button>
                            </Link>
                        </div>
                    </form>
                    <div className={classes.grow}/>
                    <div className={classes.sectionDesktop}>
                        <IconButton aria-label="show 4 new mails" color="inherit">
                            <Badge badgeContent={4} color="secondary">
                                <Mail/>
                            </Badge>
                        </IconButton>
                        <IconButton aria-label="show 17 new notifications" color="inherit">
                            <Badge badgeContent={17} color="secondary">
                                <Notifications/>
                            </Badge>
                        </IconButton>
                        <IconButton
                            edge="end"
                            aria-label="account of current user"
                            aria-haspopup="true"
                            color="inherit"
                        >
                            <AccountCircle/>
                        </IconButton>
                    </div>
                    <div className={classes.sectionMobile}>
                        <IconButton
                            aria-label="show more"
                            aria-haspopup="true"
                            color="inherit"
                        >
                            <More/>
                        </IconButton>
                    </div>
                </Toolbar>
            </AppBar>
        </div>
    )
}

export default Navbar;
