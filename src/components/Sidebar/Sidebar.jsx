import React, { useEffect, useState } from 'react'
import api from '../../util/api'
import { Button, Divider, Drawer, List, ListItem, ListItemIcon, ListItemText } from '@material-ui/core'
import { Headset, Inbox, Mail } from '@material-ui/icons'

import twitchLogo from '../../assets/Logo/twitch_logo.svg'
import TopStream from '../List/TopStream'
import { Link } from 'react-router-dom'

function Sidebar ({ className, variant, classes, anchor, link }) {

    const [topStreams, setTopStreams] = useState([])

    useEffect(() => {
        const fetchData = async () => {
            const result = await api.get('https://api.twitch.tv/helix/streams')
            let dataArray = result.data.data
            //console.log(result);

            let gameIDs = dataArray.map(stream => {
                return stream.game_id;
            });

            let userIDs = dataArray.map(stream => {
                return stream.user_id;
            });
            //console.log(gameIDs, userIDs);

            let baseUrlGames = "https://api.twitch.tv/helix/games?";
            let baseUrlUsers = "https://api.twitch.tv/helix/users?";

            let queryParamsGame = "";
            let queryParamsUser = "";

            gameIDs.map(id => {
                return (queryParamsGame = queryParamsGame + `id=${id}&`)
            });

            userIDs.map(id => {
                return (queryParamsUser = queryParamsUser + `id=${id}&`)
            });

            let finalGameUrls = baseUrlGames + queryParamsGame;
            let finalUserUrls = baseUrlUsers + queryParamsUser;

            //console.log(finalGameUrls, finalUserUrls);

            let gameNames = await api.get(finalGameUrls);
            let getUsers = await api.get(finalUserUrls);

            let gamesNameArray = gameNames.data.data;
            let usersArray = getUsers.data.data;

            //console.log(gamesNameArray, usersArray)

            let finalArray = dataArray.map(stream => {
                stream.game_name = "";
                stream.user_avatar = "";
                stream.box_art_url = "";
                stream.login = "";

                gamesNameArray.forEach(name => {
                    usersArray.forEach(user => {
                        if (stream.user_id === user.id && stream.game_id === name.id) {
                            stream.user_avatar = user.profile_image_url;
                            stream.box_art_url = name.box_art_url
                                .replace("{width}", "188")
                                .replace("{height}", "250");
                            stream.game_name = name.name;
                            stream.login = user.login
                        }
                    })
                });
                return stream
            });

            setTopStreams(finalArray.slice(0, 6))
        };

        fetchData();
    }, []);

    return (
        <React.Fragment>
            <Drawer
                className={className}
                variant={variant}
                classes={classes}
                anchor={anchor}
            >
                <div className={classes.toolbar} style={{
                    overflow: "hidden",
                    backgroundColor: "#6641A6",
                    position: "fixed",
                    top: "0",
                    width: "240px",
                    zIndex: 1,
                    textAlign: "center",
                    padding: "17px 0"
                }}><Link to="/"><img style={{margin: "0 auto"}} src={twitchLogo} width="100px"
                                     alt="Twitch logo"/></Link></div>
                <div className={classes.toolbar} style={{marginBottom: "80px"}}/>
                <Button variant="contained"
                        style={{backgroundColor: "#52289F", color: "#fff", width: "150px", margin: "20px auto"}}
                        disableElevation>
                    <Headset className="mr-2"/>Streaming
                </Button>
                <List component="nav" aria-label="main mailbox folders">
                    <ListItem button component={Link} to="/">
                        <ListItemIcon>
                            <Inbox/>
                        </ListItemIcon>
                        <ListItemText primary="Homepage"/>
                    </ListItem>
                    <ListItem button>
                        <ListItemIcon>
                            <Mail/>
                        </ListItemIcon>
                        <ListItemText primary="Category"/>
                    </ListItem>
                    <ListItem button>
                        <ListItemIcon>
                            <Mail/>
                        </ListItemIcon>
                        <ListItemText primary="Gaming"/>
                    </ListItem>
                    <ListItem button>
                        <ListItemIcon>
                            <Mail/>
                        </ListItemIcon>
                        <ListItemText primary="Friends"/>
                    </ListItem>
                    <ListItem button>
                        <ListItemIcon>
                            <Mail/>
                        </ListItemIcon>
                        <ListItemText primary="Get bites"/>
                    </ListItem>
                    <ListItem button>
                        <ListItemIcon>
                            <Mail/>
                        </ListItemIcon>
                        <ListItemText primary="Prime"/>
                    </ListItem>
                </List>
                <Divider style={{marginBottom: 20}}/>
                <TopStream/>
            </Drawer>
        </React.Fragment>
    )
}

export default Sidebar
