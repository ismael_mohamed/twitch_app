import React from "react";
import Skeleton from "@material-ui/lab/Skeleton";
import {Grid, Paper} from "@material-ui/core";

export default function AdCardLoader() {
    return (
        <React.Fragment>
            <Grid item sm={2} xs={12}>
                <Paper className="p-3">
                    <Skeleton height={100} width="80%" style={{marginBottom: 5}}/>
                    <Skeleton height={10} width="30%"/>
                    <Skeleton height={80} width="100%"/>
                    <Grid container spacing={2}>
                        <Grid item sm={6} xs={6}>
                            <Skeleton height={35} width="50%"/>
                        </Grid>
                    </Grid>
                </Paper>
            </Grid>
        </React.Fragment>
    )
}
