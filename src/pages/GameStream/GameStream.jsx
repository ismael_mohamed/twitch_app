import React, {useEffect, useState} from "react";
import api from "../../util/api";
import {Link, useLocation, useParams} from "react-router-dom"
import {Card, CardActionArea, CardMedia, Grid, Paper, Typography} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";

function GameStream() {

    let location = useLocation();
    let {slug} = useParams();

    const [streamData, setStreamData] = useState([]);
    const [viewers, setViewers] = useState(0);

    useEffect(() => {

        const fetchData = async () => {
            const result = await api.get(`https://api.twitch.tv/helix/streams?game_id=${location.state.gameId}`);
            let dataArray = result.data.data;

            let finalArray = dataArray.map(stream => {
                stream.thumbnail_url = stream.thumbnail_url
                    .replace("{width}", "440")
                    .replace("{height}", "248");

                return stream
            });

            let totalViewers = finalArray.reduce((acc, val) => {
                return acc + val.viewer_count;
            }, 0);

            let userIds = dataArray.map(stream => {
                return stream.user_id;
            });

            let baseUrl = "https://api.twitch.tv/helix/users?";
            let queryParamsUsers = "";

            userIds.map(id => {
                return (queryParamsUsers = queryParamsUsers + `id=${id}&`)
            });

            let finalUrl = baseUrl + queryParamsUsers;
            let getUsersLogin = await api.get(finalUrl);
            let userLoginArray = getUsersLogin.data.data;

            finalArray = dataArray.map(stream => {
                stream.login = "";
                userLoginArray.forEach(login => {
                    if (stream.user_id === login.id) {
                        stream.login = login.login;
                    }
                });

                return stream
            });
            setViewers(totalViewers);
            setStreamData(finalArray)
        };

        fetchData()
    }, [location.state.gameId]);

    console.log(streamData);

    const useStyles = makeStyles({
        root: {
            maxWidth: 440,
        },
        media: {
            height: 248,
        },
    });

    const classes = useStyles();

    return (
        <React.Fragment>
            <h1 className="text-white">{slug}</h1>
            <Typography variant={"caption"} className="text-white">
                {viewers} People are currently watching
            </Typography>
            <Grid container spacing={2}>
                {streamData.map((stream, index) => (
                    <Grid item sm={3} xs={12} key={index}>
                        <Link to={{
                            pathname: `/live/${stream.login}`,
                        }}>
                            <Card className={classes.root}>
                                <CardActionArea>
                                    <CardMedia
                                        component="img"
                                        alt={stream.title}
                                        image={stream.thumbnail_url}
                                        title={stream.title}
                                    />
                                </CardActionArea>
                            </Card>
                            <Paper className="mt-2 bg-transparent" elevation={0}>
                                <Typography variant={"h6"} style={{color: "#fff", fontWeight: 800, fontSize: 13}}>
                                    {stream.user_name}
                                </Typography>
                                <Typography variant={"h6"} style={{color: "#fff", fontWeight: 400, fontSize: 13}}>
                                    {stream.title}
                                </Typography>
                                <Typography
                                    variant={"caption"}
                                    className="text-muted">
                                    {stream.viewer_count} Viewers
                                </Typography>
                            </Paper>
                        </Link>
                    </Grid>
                ))}
            </Grid>
        </React.Fragment>
    )
}

export default GameStream;
