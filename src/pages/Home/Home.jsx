import React, {useEffect, useState} from "react";
import {Divider, Grid, Link, Typography} from "@material-ui/core";
import api from "../../util/api";
import TopGame from "../../components/Games/TopGame";
import getGameData from "../../util/getGameData";
import TopStream from "../../components/Games/TopStream";


function Home() {

    const [topGames, setTopGames] = useState([]);
    const [topStreams, setTopStreams] = useState([]);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        const fetchData = async () => {
            const getGamesData = getGameData;

            const result = await getGamesData.getStreams();
            let dataArray = result;
            //console.log(result);

            let gameIDs = dataArray.map(stream => {
                return stream.game_id;
            });

            let userIDs = dataArray.map(stream => {
                return stream.user_id;
            });
            //console.log(gameIDs, userIDs);

            let baseUrlGames = getGamesData.getGames();
            let baseUrlUsers = getGamesData.getUsers();

            let queryParamsGame = "";
            let queryParamsUser = "";

            gameIDs.map(id => {
                return (queryParamsGame = queryParamsGame + `id=${id}&`)
            });

            userIDs.map(id => {
                return (queryParamsUser = queryParamsUser + `id=${id}&`)
            });

            let finalGameUrls = baseUrlGames + queryParamsGame;
            let finalUserUrls = baseUrlUsers + queryParamsUser;

            //console.log(finalGameUrls, finalUserUrls);

            let gameNames = await api.get(finalGameUrls);
            let getUsers = await api.get(finalUserUrls);

            let gamesNameArray = gameNames.data.data;
            let usersArray = getUsers.data.data;

            //console.log(gamesNameArray, usersArray)

            let finalArray = dataArray.map(stream => {
                stream.game_name = "";
                stream.user_avatar = "";
                stream.box_art_url = "";
                let thumbnailNewUrl = stream.thumbnail_url
                    .replace("{width}", "188")
                    .replace("{height}", "250");
                stream.login = "";

                gamesNameArray.forEach(name => {
                    usersArray.forEach(user => {
                        if (stream.user_id === user.id && stream.game_id === name.id) {
                            stream.user_avatar = user.profile_image_url;
                            stream.box_art_url = name.box_art_url
                                .replace("{width}", "188")
                                .replace("{height}", "250");
                            stream.thumbnail_url = thumbnailNewUrl;
                            stream.game_name = name.name;
                            stream.login = user.login
                        }
                    })
                });
                return stream
            });

            setTopGames(finalArray);
            setTopStreams(finalArray.slice(0, 5));
        };

        fetchData();
    }, []);

    return (
        <React.Fragment children>
            <div className="mb-3">
                <Grid container spacing={3}>
                    <Grid item xs={6} sm={6}>
                        <Typography variant={"h5"} style={{color: "#fff"}}>
                            Recommended categories
                        </Typography>
                    </Grid>
                    <Grid item xs={6} sm={6}>
                        <Typography variant={"body1"} style={{float: "right"}}>
                            <Link href="/games">
                                View more
                            </Link>
                        </Typography>
                    </Grid>
                </Grid>
            </div>
            <TopGame/>
            <Divider style={{margin: "20px 0"}}/>
            <div className="mb-3">
                <Grid container spacing={3}>
                    <Grid item xs={6} sm={6}>
                        <Typography variant={"h5"} style={{color: "#fff"}}>
                            Top Stream
                        </Typography>
                    </Grid>
                    <Grid item xs={6} sm={6}>
                        <Typography variant={"body1"} style={{float: "right"}}>
                            <Link href="/streams">
                                View more
                            </Link>
                        </Typography>
                    </Grid>
                </Grid>
            </div>
            <Grid container spacing={2}>
                {topGames.map((topGame, index) => (
                    <TopStream
                        key={index}
                        name={topGame.game_name}
                        viewer={topGame.viewer_count}
                        title={topGame.game_name}
                        image={topGame.box_art_url}
                    />
                ))}
            </Grid>
        </React.Fragment>
    )
}

export default Home
