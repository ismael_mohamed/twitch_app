import React, {useEffect, useState} from "react";
import {Avatar, Grid, List, ListItem, ListItemAvatar, ListItemText, makeStyles, Typography} from "@material-ui/core";
import ReactTwitchEmbedVideo from "react-twitch-embed-video";
import {useParams} from 'react-router-dom';
import api from "../../util/api"

function LiveStream() {

    const useStyles = makeStyles((theme) => ({
        root: {
            width: '100%',
            maxWidth: '36ch',
            backgroundColor: "none",
        },
        inline: {
            display: 'block',
        },
        summary: {
            overflow: 'hidden'
        }
    }));

    const classes = useStyles();

    let {slug} = useParams();
    console.log(slug);

    const [infoStream, setInfoStream] = useState([]);
    const [infoGame, setInfoGame] = useState([]);

    useEffect(() => {

        const fetchData = async () => {

            const result = await api.get(`https://api.twitch.tv/helix/streams?user_login=${slug}`);
            // console.log(result);

            if(result.data.data.length === 0) {
                setInfoStream(false)
            } else {


                let gameID = result.data.data.map(gameid => {
                    return gameid.game_id;
                });

                const resultNomGame = await api.get(`https://api.twitch.tv/helix/games?id=${gameID}`);
                // console.log(resultNomGame);

                let nomJeu = resultNomGame.data.data.map(gameName => {
                    return gameName.name;
                });


                setInfoGame(nomJeu);
                setInfoStream(result.data.data[0])
            }
        };

        fetchData();

    }, [slug]);

    console.log(infoStream);

    return (
        <React.Fragment>
            <Grid container>
                {infoStream ?
                    <Grid item sm={12}>
                        <Grid container>
                            <Grid item sm={12}>
                                <List className={classes.root}
                                      style={{paddingTop: 0, paddingBottom: 0, marginBottom: 15}}>
                                    <ListItem alignItems="flex-start"
                                              style={{paddingTop: 0, paddingBottom: 0, paddingLeft: 0}}>
                                        <ListItemAvatar>
                                            <Avatar style={{backgroundColor: "#9A80CD"}}/>
                                        </ListItemAvatar>
                                        <ListItemText
                                            style={{color: "#fff", fontWeight: "800 !important"}}
                                            primary={infoStream.user_name}
                                            secondary={
                                                <React.Fragment>
                                                    <Typography
                                                        component="span"
                                                        variant="caption"
                                                        className={classes.inline + ' ' + classes.summary}
                                                        style={{color: "#fff"}}
                                                    >
                                                        {infoStream.language} | {infoStream.type}
                                                    </Typography>
                                                </React.Fragment>
                                            }
                                        />
                                    </ListItem>
                                </List>
                            </Grid>
                        </Grid>
                        <ReactTwitchEmbedVideo height="457px" width="100%" channel={slug}/>
                        <Grid container className="mt-3">
                            <Grid item sm={6}>
                                <Grid container>
                                    <Grid item sm={8}>
                                        <Typography style={{ fontSize: 20 }} variant={"h2"} className="text-white">{infoGame}</Typography>
                                        <Typography
                                            className="text-white"
                                            variant={"h6"} style={{ fontSize: 16 }}>{infoStream.title}
                                        </Typography>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                    :
                    <React.Fragment>
                        <div>The gamer is offline</div>
                    </React.Fragment>
                }
            </Grid>
        </React.Fragment>
    )
}

export default LiveStream
