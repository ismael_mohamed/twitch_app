import React, { useEffect, useState } from 'react'
import api from '../../util/api'
import { Link, useParams } from 'react-router-dom'
import { Avatar, Grid, List, ListItem, ListItemAvatar, ListItemText, makeStyles, Typography } from '@material-ui/core'
import SearchError from '../../components/Errors/SearchError'

function Result () {

    const useStyles = makeStyles((theme) => ({
        root: {
            width: '100%',
            maxWidth: '36ch',
            backgroundColor: 'none',
        },
        inline: {
            display: 'block',
        },
        summary: {
            overflow: 'hidden'
        }
    }))

    let { slug } = useParams()
    const [result, setResult] = useState(true)
    const [streamers, setStreamers] = useState([])

    let cleanSearch = slug.replace(/ /g, '')

    useEffect(() => {
        const fetchData = async () => {
            const result = await api.get(`https://api.twitch.tv/helix/users?login=${cleanSearch}`)
            console.log(result)

            if (result.data.data.length === 0) {
                setResult(false)
            } else {
                setStreamers(result.data.data)
            }
        }

        fetchData()
    }, [cleanSearch])

    const classes = useStyles()

    return (
      result ?
        <React.Fragment>
            <Typography variant={'h5'} style={{ color: '#fff', marginBottom: 20 }}>
                Search result
            </Typography>
            <Grid container spacing={2}>
                <Grid item xs={12} sm={3}>
                    {streamers.map((streamer, index) => (
                      <Link to={{
                          pathname: `/live/${streamer.login}`
                      }}>
                          <List className={classes.root} key={index} style={{ paddingTop: 0, paddingBottom: 0 }}>
                              <ListItem alignItems="flex-start" button style={{ paddingTop: 0, paddingBottom: 0 }}>
                                  <ListItemAvatar>
                                      <Avatar style={{ backgroundColor: '#9A80CD' }}
                                              alt={`User avatar of ` + streamer.display_name}
                                              src={streamer.profile_image_url}/>
                                  </ListItemAvatar>
                                  <ListItemText
                                    style={{ color: '#fff', fontWeight: '800 !important' }}
                                    primary={streamer.display_name}
                                    secondary={
                                        <React.Fragment>
                                            <Typography
                                              component="span"
                                              variant="caption"
                                              className={classes.inline + ' ' + classes.summary}
                                              style={{ color: '#fff' }}
                                            >
                                                {streamer.description}
                                            </Typography>
                                            <Typography
                                              component="span"
                                              variant="caption"
                                              className={classes.inline}
                                              style={{ color: '#9A80CD' }}
                                            >
                                                {streamer.view_count} view
                                            </Typography>
                                        </React.Fragment>
                                    }
                                  />
                              </ListItem>
                          </List>
                      </Link>
                    ))}
                </Grid>
            </Grid>
        </React.Fragment>
        :
        <SearchError/>
    )
}

export default Result
