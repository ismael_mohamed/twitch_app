import React, {useEffect, useState} from "react";
import {Card, CardActionArea, CardMedia, Grid, makeStyles, Paper, Typography} from "@material-ui/core";
import getGameData from "../../util/getGameData";
import api from "../../util/api";
import {Link} from "react-router-dom";

function Stream() {
    const useStyles = makeStyles({
        root: {
            maxWidth: 345,
        },
        media: {
            height: 115,
        },
    });

    const classes = useStyles();

    const [streams, setStreams] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            const getGamesData = getGameData;

            let dataArray = await getGamesData.getStreams();
            //console.log(result);

            let gameIDs = dataArray.map(stream => {
                return stream.game_id;
            });

            let userIDs = dataArray.map(stream => {
                return stream.user_id;
            });
            //console.log(gameIDs, userIDs);

            let baseUrlGames = getGamesData.getGames();
            let baseUrlUsers = getGamesData.getUsers();

            let queryParamsGame = "";
            let queryParamsUser = "";

            gameIDs.map(id => {
                return (queryParamsGame = queryParamsGame + `id=${id}&`)
            });

            userIDs.map(id => {
                return (queryParamsUser = queryParamsUser + `id=${id}&`)
            });

            let finalGameUrls = baseUrlGames + queryParamsGame;
            let finalUserUrls = baseUrlUsers + queryParamsUser;

            //console.log(finalGameUrls, finalUserUrls);

            let gameNames = await api.get(finalGameUrls);
            let getUsers = await api.get(finalUserUrls);

            let gamesNameArray = gameNames.data.data;
            let usersArray = getUsers.data.data;

            //console.log(gamesNameArray, usersArray)

            let finalArray = dataArray.map(stream => {
                stream.game_name = "";
                stream.user_avatar = "";
                stream.box_art_url = "";
                let thumbnailNewUrl = stream.thumbnail_url
                    .replace("{width}", "188")
                    .replace("{height}", "250");
                stream.login = "";

                gamesNameArray.forEach(name => {
                    usersArray.forEach(user => {
                        if (stream.user_id === user.id && stream.game_id === name.id) {
                            stream.user_avatar = user.profile_image_url;
                            stream.box_art_url = name.box_art_url
                                .replace("{width}", "188")
                                .replace("{height}", "250");
                            stream.thumbnail_url = thumbnailNewUrl;
                            stream.game_name = name.name;
                            stream.login = user.login
                        }
                    })
                });
                return stream
            });

            setStreams(finalArray)
        };

        fetchData();
    }, []);

    return (
        <React.Fragment>
            <div className="mb-3">
                <Typography variant={"h5"} style={{color: "#fff"}}>
                    Most popular streams
                </Typography>
            </div>
            <Grid container spacing={1}>
                {streams.map((stream, index) => (
                    <Grid item xs={2} key={index} component={Link} to={{pathname: `/live/${stream.login}`}}>
                        <Card className={classes.root}>
                            <CardActionArea>
                                <CardMedia
                                    component="img"
                                    alt={stream.game_name}
                                    height="248"
                                    image={stream.box_art_url}
                                    title={stream.game_name}
                                />
                            </CardActionArea>
                        </Card>
                        <Paper className="mt-2 bg-transparent" elevation={0}>
                            <Typography variant={"h6"} style={{color: "#fff", fontWeight: 400, fontSize: 13}}>
                                {stream.game_name}
                            </Typography>
                            <Typography
                                variant={"h6"}
                                className="text-white font-weight-bold"
                                style={{fontSize: 13}}
                            >
                                By {stream.login}
                            </Typography>
                            <Typography
                                variant={"caption"}
                                className="text-muted">
                                {stream.viewer_count} Viewers
                            </Typography>
                        </Paper>
                    </Grid>
                ))}
            </Grid>
        </React.Fragment>
    )
}

export default Stream
