import axios from "axios";

const CLIENT_ID = ''
let api = axios.create({
    headers: {
        'Client-ID': CLIENT_ID,
    }
});

export default api;
