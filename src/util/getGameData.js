import api from "./api";

function getStreams() {
    return api.get('https://api.twitch.tv/helix/streams')
        .then(response => response.data.data);
}

function getGames() {
    return 'https://api.twitch.tv/helix/games?'
}

function getUsers() {
    return 'https://api.twitch.tv/helix/users?'
}

export default {
    getGames,
    getStreams,
    getUsers
}
